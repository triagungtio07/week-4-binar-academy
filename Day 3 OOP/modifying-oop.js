class Car {
    constructor(){
        this.brand = "Suzuki"
    }

    starEngine(){
        console.log('mesin menyala')
    }
}

const car = new Car();
//Modifying dan Overriding

car.brand = 'Honda'
car.startEngine = function(){
    console.log("methodnya aku bajak")
}

console.log(car.brand)
car.startEngine()
