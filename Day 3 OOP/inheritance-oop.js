// Parent class
class Engine{
    startEngine() {
        console.log(`Mesin ${this.type} menyala`)
    }
    stopEngine() {}
}

// Sub class
class Car extends Engine {
    
    horn() {}
}

// Child class/sub class
class Toyota extends Car {
    constructor(type){
        super();
        this.type = type
    }
}
class Honda extends Car {
    constructor(type){
        super();
        this.type = type
    }
}
class Daihatsu extends Car {
    constructor(type){
        super();
        this.type = type
    }

    lamp(){
        console.log('lampu menyala')
    }
}

const avanza = new Toyota('Avanza');
const mobilio = new Honda('Honda');
const daihatsu = new Daihatsu('X-pander');

// console.log(avanza.type)
// console.log(mobilio.type)
// console.log(daihatsu.type)

// avanza.startEngine()
// mobilio.startEngine()
// daihatsu.startEngine()
avanza.startEngine()
daihatsu.lamp()