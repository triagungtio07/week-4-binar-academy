// Pewarisan hanya bisa menampung satu kelas Parent
//Jika ingin menampung lebih dari satu bauh class parent
class Car {
    checkwheel(){
        console.log('Ada 4 roda')
    }
}

class Motocycle{
    checkwheel(){
        console.log('Ada 2 roda')
    }
}
class Engine {
    check(){
        console.log('Engine is fine ')
    }
}

class Honda {
    constructor(type, car , motocycle, engine){
        this.type = type;
        this.car = depedency.car;
        this.motocycle = depedency.motocycle;
        this.engine = depedency.engine;
    }
}
const car = new Car()
const motocycle = new Motocycle()
const engine = new Engine()

const depedency = {car, motocycle, engine}

// depedency injection
const mobilio = new Honda('Mobilio', car , motocycle, engine)

// mobilio.car.checkwheel()
// mobilio.motocycle.checkwheel()
mobilio.engine.check()