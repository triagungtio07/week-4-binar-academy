class Car {
    // Static Property
    static brand2 = 'Honda';

    constructor() {
        //Instance Property
        this.brand = 'Toyota';
        this.type = 'Honda';
    }
    // Instance Method
    startEngine() {
        console.log('mobil menyalakan mesin')
    }
    
    //Static Method
    static getBrand2(){
        return Car.brand2;
    }

}

const car = new Car()

//instance property bisa langsung dipanggil
console.log(car.brand);
car.startEngine


/* jika ingin mengambil static property harus ambil
 dari class atau melalui method get yang Instance tidak bisa yang static */
console.log(Car.brand2)
console.log(Car.getBrand2())