// //Identik dengan access modifier
// //produsen
// class Car {
//     constructor(){
//         //public property
//         this.brand = "Honda"
//     }

//     //Public Method
//     getBrand(){
//         console.log('brandnya adalah' + this.brand)
//     }
// }


// //client
// const car = new Car()

// console.log(car.brand)
class Bank{
    #saldo = 1000;
    getSaldo(){
        console.log(`${this.#saldo}`)
    }

    #setSaldo(saldo){
        this.#saldo += saldo
    }

    transaction(type, amount){
        if (type == 'debit'){
            this.#saldo += amount;
        }else {
            this.#saldo -= amount
        }
    }
}

class Atm extends Bank {
    constructor(){
        super()
    }
    withdraw(amount){
        this.transaction('kredit', amount)
    }
    payment(amount){
        this.transaction('kredit', amount)
    }
    deposit(amount){
        this.transaction('debit', amount)
    }
   
}

const atm = new Atm();

// atm.deposit(4000);
// // atm.#setSaldo(10000)
// atm.getSaldo();

//Saldo sebelum transaksi
atm.getSaldo()

//bayar listrik
atm.payment(500);

//ambil uang
atm.withdraw(200)

// setor tunai
atm.deposit(1000)

//Saldo setelah transaksi
atm.getSaldo()



