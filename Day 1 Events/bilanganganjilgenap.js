const Event = require("events");

//inisiasi sebuah variabel event

const evenEmitter = new Event();

function bilanganGenap(number){
    console.log(number+" bilangan genap")
    
}

function bilanganGanjil(number){
    console.log(number+" bilangan genjil")
}

evenEmitter.on('genap', bilanganGenap);
evenEmitter.on('ganjil', bilanganGanjil);


for (let index = 0; index <= 10; index++) {
    if (index%2 == 0){
        evenEmitter.emit('genap', index);
    }else{
        evenEmitter.emit('ganjil', index)
    }
    
}

