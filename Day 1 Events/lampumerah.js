const Event = require("events");

//inisiasi sebuah variabel event

const evenEmitter = new Event();


//Inisiasi function handler

function lampuMerah() {
   console.log('Kendaraan anda harus berhenti')
}

function lampuHijau() {
    console.log('Kendaraan anda harus jalan')
 }

// Registrasi sebuah event
evenEmitter.on('lampumerah', lampuMerah )
evenEmitter.on('lampuhijau', lampuHijau )



//trigger events
evenEmitter.emit('lampuhijau')





