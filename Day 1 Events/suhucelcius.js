const Event = require('events');
const evenEmitter = new Event;
// inisasi fungsi event handller
function mendidih(suhu) {
    console.log(suhu + " Air Mendidih")
}

function hangat(suhu) {
    console.log(suhu + " Air Hangat")
}

function beku(suhu) {
    console.log(suhu + " Air Beku")
}

// registrasi event 
evenEmitter.on('airMendidih', mendidih);
evenEmitter.on('airHangat', hangat);
evenEmitter.on('airBeku', beku)


for (let celcius = 200; celcius > -2; celcius--) {
    // if (celcius == 100) {
    //     evenEmitter.emit('airMendidih', celcius);
    // } else if (celcius == 50) {

    //     evenEmitter.emit('airHangat', celcius);

    // } else if (celcius == 0) {
    //     evenEmitter.emit('airBeku', celcius);
    // }
    switch (celcius) {
        case 100:
            evenEmitter.emit('airMendidih', celcius);
            break;
        case 50:
            evenEmitter.emit('airHangat', celcius);
            break;
        case 0:
            evenEmitter.emit('airHangat', celcius);
            break;

        default:
            break;
    }

}