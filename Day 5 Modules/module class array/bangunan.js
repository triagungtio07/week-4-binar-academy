class Bangunan {
    menghitungSegitiga(a, t) {
        return 0.5 * a * t;
    }
    menghitungPersegi(s) {
        return s * s;
    }

    menghitungPersegiPanjang(p, l) {
        return p * l;
    }

    menghitungLingkaran(r) {
        return 3.14 * r * r;
    }
}

const test = () => {
    console.log("ini sebuah function dari module bangunan")
}

module.exports = [Object.freeze(new Bangunan()), test]
// module export hanya bisa satu kali
// untuk memasukkan 2 fungsi yang berbeda atau dua kelas yang berbeda dapat menggunakan array