/* ecma script adalah bahasa pemograman yang sudah distandarisasi oleh ecma
*/

//es5
const messageTest /*function name */ = function (newName, newAge)/*parameter */{
    console.log(`hello nama saya ${newName}, umur saya ${newAge}`)// function expression

}

//es6

const messageTest2 = (newName,newAge) => {
    console.log(`hello nama saya ${newName}, umur saya ${newAge}`)
}

messageTest('agung',17)
messageTest2('agung', 17)