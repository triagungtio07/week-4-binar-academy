// function queueTime(customers, n) {
//   let tills = Array(n).fill(0);
//   console.log(tills)
//   customers.forEach((customer) => {
//     let nextTill = tills.indexOf(Math.min(...tills))
//     tills[nextTill] += customer;
//   });
//   console.log(tills)
//   console.log(Math.max(...tills));
//   return Math.max(...tills);
// }

// queueTime([1,2,3,4], 2)

n = 2
customers = [1, 2, 3, 4]

let tills = Array(n).fill(0);
customers.forEach((customer) => {
  let nextTill = tills.indexOf(Math.min(...tills))
  console.log(nextTill)
  tills[nextTill] += customer;
})