// class umumnya diawali dengan huruf besar

class Mobil {
    // digunakan method pembantu fungsi yang digunakan saat memulai class
    constructor(name, color) {
        this.name = name;
        this.weight = 800;
        this.model = "MPV";
        this.color = color;
    }

    start(){
        console.log(`${this.name} telah menyala`)
    }
    drive(){
        console.log(`${this.name} mobil siap dikemudikan`)
    }
    break() {
        console.log('mobil direm')
    }
    stop(){
        console.log('mobil berhenti')
    }
}

// cara memanggil class harus membuat sebuah variabel terlebih dahulu
//creat new instance
let mobil1 = new Mobil("Honda Jazz", "merah");
let mobil2 = new Mobil("Hilux", "hitam");

// console.log(mobil.name)
// mobil.drive



mobil1.start()
mobil2.start()