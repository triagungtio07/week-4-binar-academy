class Drone {
    constructor(brand, camera,speed ) {
        this.brand = brand;
        this.camera = camera;
        this.speed = speed;
    }
    startDrone(){
        console.log(`Drone ${this.brand} siap digunkan`)
        this.changeSpeed(500)
        console.log(`Kecepatan ${this.speed} rpm`)
        return this
    }
    stopDrone(){
        console.log(`Drone ${this.brand} telah berhenti`)
        this.changeSpeed(0)
        console.log(`Kecepatan ${this.speed} rpm`)
        return this
    }
    changeSpeed(newSpeed){
        this.speed = newSpeed;
        return this
    }
    cameraOn() {
        console.log('Kamera Menyala')
        this.camera = true;
        return this
    }
    cameraOff(){
        console.log('Kamera Mati')
        this.camera = false;
        return this
    }

    changeBrand(newBrand){
        this.brand = newBrand
        return this
    }

}

const drone =  new Drone("Dji", false, 0 )

console.log(drone)

drone.startDrone().cameraOn()


